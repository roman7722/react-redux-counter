import { CounterActionType, TCounterActions } from "./actions.types";
import { Action } from "redux";
import { ICounterState } from "../../containers/Counter.types";

const initialState: ICounterState = {
  count: 0
}

const counterReducer = (state: ICounterState = initialState, action: Action | TCounterActions): ICounterState => {
  switch (action.type) {
    case CounterActionType.INCREMENT:
      return { ...state, count: state.count + 1 };
    case CounterActionType.DECREMENT:
      return { ...state, count: state.count - 1 };
    case CounterActionType.ASYNC_INCREMENT:
      return { ...state, count: state.count + 1 };
    default:
      return state;
  }
}

export default counterReducer;
