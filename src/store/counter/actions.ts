import { CounterActionType, IIncrementAction, IDecrementAction, TAsyncIncrementAction } from "./actions.types";
import { Dispatch } from 'redux';

export const incrementCounterAction = (): IIncrementAction => ({
    type: CounterActionType.INCREMENT
});

export const decrementCounterAction = (): IDecrementAction => ({
    type: CounterActionType.DECREMENT
});

export const asyncIncrementCounterAction = (): TAsyncIncrementAction => {
  return function (dispatch: Dispatch) {
    setTimeout(() => {
      dispatch({type: CounterActionType.ASYNC_INCREMENT})
    }, 2000);
  }
};
