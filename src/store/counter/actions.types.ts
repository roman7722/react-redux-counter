import { Action } from "redux";
import { Dispatch } from 'redux';

export enum CounterActionType {
  INCREMENT = "INCREMENT",
  DECREMENT = "DECREMENT",
  ASYNC_INCREMENT = "ASYNC_INCREMENT"
}

export interface IIncrementAction extends Action {
  type: CounterActionType.INCREMENT;
}

export interface IDecrementAction extends Action {
  type: CounterActionType.DECREMENT;
}

export interface IAsyncIncrementAction extends Action {
    type: CounterActionType.ASYNC_INCREMENT;
  }

export type TAsyncIncrementAction = (dispatch: Dispatch) => void;

export type TCounterActions = IIncrementAction | IDecrementAction | IAsyncIncrementAction;
