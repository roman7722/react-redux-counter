import { createStore, applyMiddleware, combineReducers, Middleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import counterReducer from "./counter/counterReducer";
import thunc from 'redux-thunk';
import { ICounterState } from '../containers/Counter.types';

const rootReducer = combineReducers({
  counterReducer
});

const middleware: Middleware[] = [thunc];

export interface IState {
  counterReducer: ICounterState;
};

const configureStore = (initialState?: IState) => {
  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
  );
};

const store = configureStore();

export default store;
