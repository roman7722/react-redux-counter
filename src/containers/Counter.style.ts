export const counterStyle = { 
  boxSizing: "border-box", 
  marginTop: "50px",
  maxWidth: "350px",
  border: "1px solid #eee",
} as const;

export const buttonStyle = { 
  minWidth: "90px",
  marginRight: "-30px"
} as const;
