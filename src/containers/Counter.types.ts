export interface ICounterState {
  count: number;
}

export interface ICounterProps {
  readonly increment: () => void;
  readonly decrement: () => void;
  readonly asyncInc: () => void;
  readonly counter: ICounterState;
}
