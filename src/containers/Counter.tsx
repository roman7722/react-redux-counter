import React from 'react';
import { Dispatch, bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { incrementCounterAction, decrementCounterAction, asyncIncrementCounterAction } from '../store/counter/actions';
import { IState } from '../store/index';
import { ICounterProps } from './Counter.types';
import { counterStyle, buttonStyle } from './Counter.style';

class Counter extends React.Component<ICounterProps> {
  render() {
    const { increment, decrement, asyncInc, counter } = this.props;

    return (
      <div className="container shadow-sm p-3 mb-5 bg-white rounded" style={counterStyle}>
        <div className="row">
          <div className="col-sm">
            <input
              type='text'
              className="form-control"
              style={{textAlign: 'right' }}
              value={counter?.count}
              readOnly={true}
            />
          </div>
        </div>
        <div className="row" style={{ marginTop: "10px" }}>
          <div className="col-auto mr-auto">
            <button className="btn btn-success" onClick={increment} style={buttonStyle}>
              Inc
            </button>
          </div>
          <div className="col-auto mr-auto">
            <button className="btn btn-danger" onClick={decrement} style={buttonStyle} >
              Dec
            </button>
          </div>
          <div className="col-auto">
            <button className="btn btn-warning" onClick={asyncInc} >
              Async Inc (2c)
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: IState) => {
  return {
    counter: state.counterReducer
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  increment: bindActionCreators(incrementCounterAction, dispatch),
  decrement: bindActionCreators(decrementCounterAction, dispatch),
  asyncInc: bindActionCreators(asyncIncrementCounterAction, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
